# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* order_design_ext
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 17.0+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-06-04 07:45+0000\n"
"PO-Revision-Date: 2024-06-04 07:45+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.report_saleorder_document_inherit
msgid "27.00"
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.report_saleorder_document_inherit
msgid "31.05"
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.report_saleorder_document_inherit
msgid "<span>Amount</span>"
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.report_saleorder_document_inherit
msgid "<span>Disc.%</span>"
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.report_saleorder_document_inherit
msgid "<strong class=\"mr16\">Subtotal</strong>"
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.view_base_document_inherit
msgid "<strong>Layout Spesific Field</strong>"
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.view_base_document_inherit
msgid "<strong>Title CSS</strong>"
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.report_saleorder_document_inherit
msgid "A note, whose content usually applies to the section or product above."
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.report_saleorder_document_inherit
msgid "A section title"
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.view_order_form_inherit
#: model_terms:ir.ui.view,arch_db:order_design_ext.view_order_template_form_inherit
msgid "Add a i-section"
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.view_order_form_inherit
#: model_terms:ir.ui.view,arch_db:order_design_ext.view_order_template_form_inherit
msgid "Add a page break"
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.view_order_form_inherit
#: model_terms:ir.ui.view,arch_db:order_design_ext.view_order_template_form_inherit
msgid "Add a title"
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.report_saleorder_document_inherit
msgid "Bacon Burger"
msgstr ""

#. module: order_design_ext
#: model:ir.model,name:order_design_ext.model_res_company
msgid "Companies"
msgstr "Sociétés"

#. module: order_design_ext
#: model:ir.model,name:order_design_ext.model_base_document_layout
msgid "Company Document Layout"
msgstr "Mise en page des documents de votre société"

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.report_saleorder_document_inherit
msgid "Description"
msgstr ""

#. module: order_design_ext
#: model:ir.model.fields,field_description:order_design_ext.field_sale_order_line__display_type
#: model:ir.model.fields,field_description:order_design_ext.field_sale_order_template_line__display_type
msgid "Display Type"
msgstr "Type d'affichage"

#. module: order_design_ext
#: model:ir.model.fields.selection,name:order_design_ext.selection__sale_order_line__display_type__line_isection
#: model:ir.model.fields.selection,name:order_design_ext.selection__sale_order_template_line__display_type__line_isection
msgid "Isection"
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.report_saleorder_document_inherit
msgid "Order N°"
msgstr "Commande N°"

#. module: order_design_ext
#: model:ir.model.fields.selection,name:order_design_ext.selection__sale_order_line__display_type__line_pagebreak
#: model:ir.model.fields.selection,name:order_design_ext.selection__sale_order_template_line__display_type__line_pagebreak
msgid "Page Break"
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.report_saleorder_document_inherit
msgid "Pro-Forma Invoice N°"
msgstr "Facture pro forma N°"

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.report_saleorder_document_inherit
msgid "Quantity"
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.report_saleorder_document_inherit
msgid "Quotation N°"
msgstr "Devis N°"

#. module: order_design_ext
#: model:ir.model,name:order_design_ext.model_sale_order_template_line
msgid "Quotation Template Line"
msgstr "Ligne du modèle de devis"

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.view_order_form_inherit
#: model_terms:ir.ui.view,arch_db:order_design_ext.view_order_template_form_inherit
msgid "Row Styling"
msgstr ""

#. module: order_design_ext
#: model:ir.model,name:order_design_ext.model_sale_order
msgid "Sales Order"
msgstr ""

#. module: order_design_ext
#: model:ir.model,name:order_design_ext.model_sale_order_line
msgid "Sales Order Line"
msgstr "Ligne de commande"

#. module: order_design_ext
#: model:ir.model.fields,field_description:order_design_ext.field_sale_order_line__style
#: model:ir.model.fields,field_description:order_design_ext.field_sale_order_template_line__style
#: model_terms:ir.ui.view,arch_db:order_design_ext.view_order_form_inherit
#: model_terms:ir.ui.view,arch_db:order_design_ext.view_order_template_form_inherit
msgid "Style"
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.report_saleorder_document_inherit
msgid "Tax 15%"
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.report_saleorder_document_inherit
msgid "Taxes"
msgstr ""

#. module: order_design_ext
#: model:ir.model.fields,help:order_design_ext.field_sale_order_template_line__display_type
msgid "Technical field for UX purpose."
msgstr ""

#. module: order_design_ext
#: model:ir.model.fields.selection,name:order_design_ext.selection__sale_order_line__display_type__line_title
#: model:ir.model.fields.selection,name:order_design_ext.selection__sale_order_template_line__display_type__line_title
msgid "Title"
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.view_base_document_inherit
msgid "Title CSS"
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.report_saleorder_document_inherit
msgid "Unit Price"
msgstr ""

#. module: order_design_ext
#: model:ir.model.fields,field_description:order_design_ext.field_base_document_layout__yotech_order_layout
msgid "Yotech Order Layout"
msgstr ""

#. module: order_design_ext
#: model:ir.model.fields,help:order_design_ext.field_base_document_layout__title_css
#: model:ir.model.fields,help:order_design_ext.field_res_company__title_css
msgid "css for title section in report"
msgstr ""

#. module: order_design_ext
#: model:ir.model.fields,field_description:order_design_ext.field_base_document_layout__title_css
#: model:ir.model.fields,field_description:order_design_ext.field_res_company__title_css
msgid "title css"
msgstr ""

#. module: order_design_ext
#: model_terms:ir.ui.view,arch_db:order_design_ext.report_saleorder_document_inherit
msgid "units"
msgstr ""