
//sale.action_quotations_with_onboarding

odoo.define('order_design_ext.tour_sale', function(require) {
    "use strict";

    var core = require('web.core');
    var tour = require('web_tour.tour');

    var _t = core._t;

    // own template step
    function addTitle(title) {
        return [
            {
                trigger: ".o_field_x2many_list_row_add > a:nth-child(5)",
                run: 'click',
                auto: true,
                content: _t("Click here to add title."),
                position: "bottom",
            },{
                trigger: ".o_field_one2many[name=order_line] .o_selected_row .o_field_widget[name=name]",
                extra_trigger: ".o_sale_order",
                content: _t("Write a title."),
                position: "top",
                run: function(actions){
                    let $input = this.$anchor.find("input");
                    actions.text(`${title}`, $input.length === 0 ? this.$anchor : $input);
                    var keyDownEvent = jQuery.Event("keydown");
                    keyDownEvent.which = 13;
                    this.$anchor.trigger(keyDownEvent);
                }
            },
        ]
    }

    function addProduct(name, price) {
        return [
            {
                trigger: ".o_field_x2many_list_row_add > a",
                extra_trigger: ".o_field_many2one[name='partner_id'] .o_external_button",
                content: _t("Click here to add some products or services to your quotation."),
                position: "bottom",
            }, {
                trigger: ".o_field_widget[name='product_id'], .o_field_widget[name='product_template_id']",
                extra_trigger: ".o_sale_order",
                content: _t("Select a product, or create a new one on the fly."),
                position: "right",
                run: function (actions) {
                    var $input = this.$anchor.find("input");
                    actions.text("DESK0001", $input.length === 0 ? this.$anchor : $input);
                    // fake keydown to trigger search
                    var keyDownEvent = jQuery.Event("keydown");
                    keyDownEvent.which = 42;
                    this.$anchor.trigger(keyDownEvent);
                    var $descriptionElement = $(".o_form_editable textarea[name='name']");
                    // when description changes, we know the product has been created
                    $descriptionElement.change(function () {
                        $descriptionElement.addClass("product_creation_success");
                    });
                },
                id: "product_selection_step"
            }, {
                trigger: ".ui-menu.ui-widget .ui-menu-item a:contains('DESK0001')",
                auto: true,
            }, {
                trigger: ".o_form_editable textarea[name='name'].product_creation_success",
                auto: true,
                run: function () {
                } // wait for product creation
            }, {
                trigger: ".o_field_widget[name='price_unit'] ",
                extra_trigger: ".o_sale_order",
                content: _t("<b>Set a price</b>."),
                position: "right",
                run: "text 10.0"
            }
        ]
    }

    tour.register("order_design_ext_quote", {
        url: "/web#action=sale.action_quotations_with_onboarding&view_type=form",
        rainbowMan: true,
        rainbowManMessage: "<b>Congratulations</b>, your first quotation is sent!<br>Check your email to validate the quote.",
        sequence: 30,
    }, [{
        trigger: ".o_form_editable .o_field_many2one[name='partner_id']",
        extra_trigger: ".o_sale_order",
        content: _t("Write a company name to create one, or see suggestions."),
        position: "bottom",
        run: function (actions) {
            actions.text("Agrolait", this.$anchor.find("input"));
        },
    }, {
        trigger: ".ui-menu-item > a",
        auto: true,
        in_modal: false,
    }, 
    ...addTitle('Title 1'),
    ...addProduct('DESK0002', '15'),
    ...addProduct('DEKS0002', '20'),
    ...addProduct('DEKS0002', '20'),
    ]);

});