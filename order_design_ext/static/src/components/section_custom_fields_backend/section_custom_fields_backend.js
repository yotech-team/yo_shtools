/** @odoo-module **/

import { registry } from "@web/core/registry";
import { ListRenderer } from "@web/views/list/list_renderer";
import { X2ManyField, x2ManyField } from "@web/views/fields/x2many/x2many_field";
import { TextField, ListTextField } from "@web/views/fields/text/text_field";
import { CharField } from "@web/views/fields/char/char_field";
import { standardFieldProps } from "@web/views/fields/standard_field_props";
import { Component, useEffect } from "@odoo/owl";

export class SectionCustomListRenderer extends ListRenderer {
    /**
     * The purpose of this extension is to allow sections and notes in the one2many list
     * primarily used on Sales Orders and Invoices
     *
     * @override
     */
    setup() {
        super.setup();
        this.titleField = "name";
        this.styleField = "style";
        useEffect(
            () => this.focusToName(this.props.list.editedRecord),
            () => [this.props.list.editedRecord]
        )
    }

    focusToName(editRec) {
        if (editRec && editRec.isNew && this.isSectionOrNote(editRec)) {
            const col = this.state.columns.find((c) => c.name === this.titleField);
            this.focusCell(col, null);
        }
    }

    isPageBreak(record=null) {
        record = record || this.record;
        return ['line_pagebreak'].includes(record.data.display_type);        
    }

    isTitle(record = null) {
        record = record || this.record;
        return ['line_title'].includes(record.data.display_type);
    }

    isSectionOrNote(record=null) {
        record = record || this.record;
        return ['line_isection','line_section', 'line_note'].includes(record.data.display_type);
    }

    getRowClass(record) {
        const existingClasses = super.getRowClass(record);
        return `${existingClasses} o_is_${record.data.display_type}`;
    }

    getCellClass(column, record) {
        // Handle hide unused columen
        const classNames = super.getCellClass(column, record);

        if (column.widget === "handle") {
            return classNames;
        }

        if (this.isSectionOrNote(record)) {
            if (column.name !== this.titleField){
                return `${classNames} o_hidden`;
            }
        }

        if (this.isPageBreak(record)) {
            return `${classNames} o_readonly`;
        }


        if (this.isTitle(record)) {
            if (column.name !== this.titleField && column.name !== this.styleField) {
                return `${classNames} o_hidden`;
            }
        }

        return classNames;
    }

    isCellReadonly(column, record) {
        const res = super.isCellReadonly(column, record);
        if(this.isPageBreak(record)){
            return true;
        }

        return res;
    }

    getColumns(record) {
        const columns = super.getColumns(record);
        if (this.isSectionOrNote(record)) {
            return this.getSectionColumns(columns);
        }
        if (this.isPageBreak(record)) {
            return  this.getSectionColumns(columns);
        }
        if (this.isTitle(record)) {
            return this.getTitleColumns(columns);
        }
        return columns;
    }

    getSectionColumns(columns) {
        
        const sectionCols = columns.filter((col) => col.widget === "handle" || col.type === "field" && col.name === this.titleField);
        const res = sectionCols.map((col) => {
            if (col.name === this.titleField) {
                return { ...col, colspan: columns.length - sectionCols.length + 1 };
            } else {
                return { ...col };
            }
        });
        return res
    }

    getTitleColumns(columns) {
        const sectionCols = columns.filter((col) => col.widget == "handle" || (col.type == "field" && (col.name == this.titleField || col.name == this.styleField)))
        // Calculate title and style colspan
        const title_length = Math.ceil(columns.length / 2)
        const style_length = columns.length - title_length - 1

        const res = sectionCols.map((col) => {
            if (col.name === this.titleField) {
                return { ...col, colspan: title_length};
            }
            else if (col.name === this.styleField) {
                return { ...col, colspan: style_length};
            }
            else {
                return { ...col };
            }
        });
        return res

    }
}
SectionCustomListRenderer.template = "order_design_ext.sectionCustomListRenderer";

export class SectionCustomFieldOne2Many extends X2ManyField {}
SectionCustomFieldOne2Many.components = {
    ...X2ManyField.components,
    ListRenderer: SectionCustomListRenderer,
};

export class SectionCustomText extends Component {
    static props = { ...standardFieldProps};

    get componentToUse() {
        let displayType = this.props.record.data.display_type;
        let charUseField = ['line_section', 'line_isection', 'line_title','line_pagebreak'];
        if(displayType == ''){
            return CharField;
        }
        if(charUseField.includes(displayType)){
            return CharField
        }else{
            return TextField
        };
    }

}
SectionCustomText.template = "order_design_ext.SectionCustomText";

export class ListSectionCustomText extends SectionCustomText {
    static props = { ...standardFieldProps};

    get componentToUse() {
        let charUseField = ['line_section', 'line_isection', 'line_title','line_pagebreak'];
        return !charUseField.includes(this.props.record.data.display_type)
            ? ListTextField
            : super.componentToUse;
    }
}

export const sectionCustomFieldOne2Many = {
    ...x2ManyField,
    component: SectionCustomFieldOne2Many,
    additionalClasses: [...x2ManyField.additionalClasses || [], "o_field_one2many"],
};

export const sectionCustomText = {
    component: SectionCustomText,
    additionalClasses: ["o_field_text"],
};

export const listSectionCustomText = {
    ...sectionCustomText,
    component: ListSectionCustomText,
};

registry.category("fields").add("section_custom_one2many", sectionCustomFieldOne2Many);
registry.category("fields").add("section_custom_text", sectionCustomText);
registry.category("fields").add("list.section_custom_text", listSectionCustomText);
