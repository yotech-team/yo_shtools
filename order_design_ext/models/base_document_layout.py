# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools


class BaseDocumentLayout(models.TransientModel):
    _inherit = "base.document.layout"

    yotech_order_layout = fields.Boolean(string="Yotech Order Layout")
    title_css = fields.Text(related="company_id.title_css", readonly=False)
