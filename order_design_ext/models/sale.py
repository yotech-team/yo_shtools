from odoo import api, models, fields


class SaleOrder(models.Model):
    _inherit = "sale.order"

    def _compute_line_data_for_template_change(self, line):
        return {
            "style": line.style,
            "display_type": line.display_type,
            "name": line.name,
            "state": "draft",
        }

    @api.onchange("sale_order_template_id")
    def _onchange_sale_order_template_id(self):
        super()._onchange_sale_order_template_id()
        template = self.sale_order_template_id.with_context(lang=self.partner_id.lang)
        for line, tline in zip(self.order_line, template.sale_order_template_line_ids):
            line.style = tline.style
            line.display_type = tline.display_type

    def _get_order_lines_to_report_docs(self):
        lines = super()._get_order_lines_to_report()

        # For document version, we split the bill to as many as report are
        tables = []
        records = self.env["sale.order.line"]

        for line in lines:
            records += line

            if line.display_type == "line_pagebreak":
                tables.append(records)
                records = self.env["sale.order.line"]
                continue

        if len(records) != 0:
            tables.append(records)

        return tables


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    display_type = fields.Selection(
        selection_add=[
            ("line_isection", "Isection"),
            ("line_pagebreak", "Page Break"),
            ("line_title", "Title"),
        ],
        ondelete={
            "line_isection": "set default",
            "line_pagebreak": "set default",
            "line_title": "set default",
        },
    )

    style = fields.Char(string="Style")


class SaleOrderTemplateLine(models.Model):
    _inherit = "sale.order.template.line"

    style = fields.Char(readonly=False, string="Style")
    display_type = fields.Selection(
        selection_add=[
            ("line_isection", "Isection"),
            ("line_pagebreak", "Page Break"),
            ("line_title", "Title"),
        ],
        ondelete={
            "line_isection": "set default",
            "line_pagebreak": "set default",
            "line_title": "set default",
        },
        default=False,
        help="Technical field for UX purpose.",
    )
