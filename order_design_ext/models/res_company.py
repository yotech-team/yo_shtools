# -*- coding: utf-8 -*-


from odoo import models, fields, api

import logging
_logger = logging.getLogger(__name__)


class ResCompany(models.Model):
    _inherit = 'res.company'

    title_css = fields.Text(string="title css",help="css for title section in report",readonly=False)