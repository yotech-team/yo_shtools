# Order Design Ext

This module extends the functionality of Order Design to support the following:
- Add new display type (title and isection) in sale.order.line and sale.order.template.line
- Add page break functionality in sale.order.line and sale.order.template.line
- Custom report layout for quotaion and order
- Add a new field to the document layout setting to allow the user to input default css for title.

## Installation

Please read the manifest for detail requirements.

## Contact

Please contact yotech for any question.