# -*- coding: utf-8 -*-
{
    "name": "Custom element in Report",
    "summary": """
        Modification in Quotation Report.""",
    "description": """
        Modification in Quotation Report by adding multiple element at sale order line level, including:
        - title : Title that support extra styling
        - i section : Section of some item
        - page break : Page break
    """,
    "author": "YOTECH team",
    "company": "Yotech SOFTWARE",
    "website": "http://www.yotech.pro",
    "category": "sale",
    "version": "17.0.1.0.0",
    "depends": ["sale", "sale_management", "web", "resource"],
    "data": [
        "views/sale_order_view.xml",
        "views/sale_order_template_view.xml",
        "views/document_view.xml",
        "reports/sale_order_report.xml",
    ],
    "assets": {
        "web.assets_backend": [
            "order_design_ext/static/src/components/**/*",
        ],
    },
    "installable": True,
    "license": "OPL-1",
}
