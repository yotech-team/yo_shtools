# -*- coding: utf-8 -*-

{
    "name": "Subscription Period Description",
    "author": "Team Yotech",
    "version": "17.0.0.0.1",
    "category": "custom",
    "license": "AGPL-3",
    "depends": ["sale_management", "sale_subscription"],
    "data": [
        'views/sale_order_views.xml',
    ],
    "auto_install": False,
}
