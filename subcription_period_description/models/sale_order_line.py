from odoo import models


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    def _prepare_invoice_line(self, **optional_values):
        self.ensure_one()
        res = super()._prepare_invoice_line(**optional_values)
        if self.display_type:
            return res

        # Change default behavior
        if self.order_id.plan_id and self.recurring_invoice:

            # Change description based on template
            description = None
            if self.order_id.display_recurring_description in ('none', 'invoice'):
                description = "%s" % (self.name)
            elif self.order_id.use_recurring_description:
                description = "%s - %s" % (self.name, self.order_id.recurring_description)

            if description:
                res['name'] = description

        return res
