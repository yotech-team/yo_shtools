from dateutil.relativedelta import relativedelta

from odoo import fields, models, _
from odoo.tools import format_date


class SaleOrder(models.Model):
    _inherit = "sale.order"

    display_recurring_description = fields.Selection([
        ('none', 'None'),
        ('line', 'Per line'),
        ('invoice', 'Per invoice'),
        ('both', 'Per line and invoice'),
    ], string="Display Reccuring Description", default="none", required=True)
    use_recurring_description = fields.Boolean("Use Custom Description", default=False)
    recurring_description = fields.Char("Reccuring Description")

    def _create_invoices(self, grouped=False, final=False, date=None):
        moves = super()._create_invoices(grouped=grouped, final=final, date=date)

        # per invoice reccurence
        for move in moves:
            if self.display_recurring_description in ('invoice', 'both'):
                narration = move.narration or ""
                if self.use_recurring_description:
                    description = "%s - %s" % (narration, self.recurring_description)

                else:
                    lang_code = self.partner_id.lang
                    if self.subscription_state == '7_upsell':
                        new_period_start = max(self.start_date or fields.Date.today(), self.first_contract_date)
                        next_invoice_date = self.next_invoice_date - relativedelta(days=1)
                    else:
                        new_period_start = self.next_invoice_date
                        default_next_invoice_date = new_period_start + self.plan_id.billing_period
                        next_invoice_date = default_next_invoice_date - relativedelta(days=1)

                    duration = self.plan_id.billing_period_display
                    format_start = format_date(self.env, new_period_start, lang_code=lang_code)
                    format_next = format_date(self.env, next_invoice_date, lang_code=lang_code)
                    start_to_next = _("\n%s to %s", format_start, format_next)
                    description = f"{narration} - {duration}{start_to_next}"

                move.narration = description

        return moves
