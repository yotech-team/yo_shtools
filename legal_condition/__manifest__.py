# -*- coding: utf-8 -*-

{
    "name": "Sales legal conditions",
    "description": """
    Legal conditions for sales report
    Add multiple option of legal condition that can be choose from sale order.
    However only one legal condition can be select from sale order.
    Selected legal condition will appers inside quotation/sale order report.
    """,
    "author": "Team Yotech",
    "version": "17.0.1.1.0",
    "category": "custom",
    "sequence": 32,
    "license": "AGPL-3",
    "images": [],
    "depends": ["sale", "sale_management"],
    "data": [
        "security/ir.model.access.csv",
        "views/legal.xml",
        "report/sale_report_saleorder_document.xml",
        "views/sale_order_form.xml",
    ],
    "demo": [
        "demo/legal_condition.xml",
    ],
    "active": True,
    "auto_install": False,
    "qweb": [],
}
