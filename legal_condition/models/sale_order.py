# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

import logging

_logger = logging.getLogger(__name__)


class sale_order(models.Model):
    _inherit = "sale.order"

    @api.model
    def _default_legal_condition(self):
        default_legal = self.env["legal.condition"].search(
            [("default", "=", True)],
            limit=1,
        )
        return default_legal.id

    legal_condition_id = fields.Many2one(
        "legal.condition",
        string="Legal Conditions",
        default=_default_legal_condition,
    )

    legal_text = fields.Text("Legal Text")

    @api.onchange("legal_condition_id")
    def onchange_legal_text(self):
        if self.legal_condition_id:
            self.legal_text = self.legal_condition_id.legal_condition
