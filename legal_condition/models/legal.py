# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class legal_condition(models.Model):
    _name = "legal.condition"
    _description = "Legal"
    _rec_name = "name"

    name = fields.Char(string="Name", required=True)
    legal_condition = fields.Char(string="Legal Condition")
    default = fields.Boolean(string="Default")

    def _set_default(self):
        self.ensure_one()
        self.env.cr.execute(
            'UPDATE legal_condition SET "default" = false WHERE "default" = true'
        )
        self.default = True

    def action_set_default(self):
        self._set_default()
        return {"type": "ir.actions.client", "tag": "reload"}
