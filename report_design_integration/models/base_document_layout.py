# -*- coding: utf-8 -*-
from PIL import Image

from odoo import api, fields, models, tools

from odoo.modules import get_resource_path

class BaseDocumentLayout(models.TransientModel):
    _inherit = 'base.document.layout'

    company_id = fields.Many2one('res.company', default=lambda self: self.env.company, required=True)
    report_footer_col1 = fields.Html(related='company_id.report_footer_col1', readonly=False, help="Use / to add html element")
    report_footer_col2 = fields.Html(related='company_id.report_footer_col2', readonly=False, help="Use / to add html element")
    report_footer_col3 = fields.Html(related='company_id.report_footer_col3', readonly=False, help="Use / to add html element")
