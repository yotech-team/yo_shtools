# -*- coding: utf-8 -*-


from odoo import models, fields, api

import logging

_logger = logging.getLogger(__name__)


class ResCompany(models.Model):
    _inherit = "res.company"

    header_image = fields.Binary(
        string="Header Image",
        readonly=False,
        help="This field image size limited to 1024x1024px",
    )
    background_image = fields.Binary(
        string="Report Background Image",
        readonly=False,
        help="This field image size limited to 1024x1024px",
    )
    footer_image = fields.Binary(
        string="Footer Image",
        readonly=False,
        help="This field image size limited to 1024x1024px",
    )
    report_footer_col1 = fields.Html(
        string="Footer text first column",
        readonly=False,
        help="This field has text for first column",
    )
    report_footer_col2 = fields.Html(
        string="Footer text second column",
        readonly=False,
        help="This field has text for second column",
    )
    report_footer_col3 = fields.Html(
        string="Footer text third column",
        readonly=False,
        help="This field has text for third column",
    )
