# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools
from PIL import Image
from PIL import Image
DEFAULT_PRIMARY = '#000000'
DEFAULT_SECONDARY = '#000000'


class BaseDocumentLayout(models.TransientModel):
    _inherit = 'base.document.layout'

    header_image = fields.Binary(
        related='company_id.header_image',
        readonly=False,
        help='This field image size limited to 1024x1024px')
    footer_image = fields.Binary(
        related='company_id.footer_image',
        readonly=False,
        help='This field image size limited to 1024x1024px')
    background_image = fields.Binary(
        related='company_id.background_image',
        readonly=False,
        help='This field image size limited to 750x750px')

    yotech_layout = fields.Boolean(string='Yotech Layout')
    yotech_back_layout = fields.Boolean(string='Background Layout')

    @api.onchange('header_image', 'footer_image', 'background_image')
    def _onchange_custom_colors(self):
        for wizard in self:
            if wizard.header_image or wizard.footer_image or wizard.background_image:
                wizard_layout = wizard.env["report.layout"].search([
                    ('view_id.name', '=', 'external_layout_background')
                ])
                wizard.report_layout_id = wizard_layout or wizard_layout.search(
                    [], limit=1)
                wizard_layout = wizard.env["report.layout"].search([
                    ('view_id.name', '=', 'external_layout_yotech_design')
                ])
                wizard.report_layout_id = wizard_layout or wizard_layout.search(
                    [], limit=1)

    @api.onchange('report_layout_id')
    def _onchange_report_layout_id(self):
        for wizard in self:
            wizard.external_report_layout_id = wizard.report_layout_id.view_id
            if wizard.report_layout_id.view_id.name == 'external_layout_clean_inherit':
                wizard.primary_color = "#98af77"
                wizard.secondary_color = "#744b27"
            else:
                wizard.primary_color = DEFAULT_PRIMARY
                wizard.secondary_color = DEFAULT_SECONDARY

            if wizard.report_layout_id:
                if wizard.report_layout_id.name == 'Yotech Design 01':
                    wizard.yotech_layout = True
                elif wizard.report_layout_id.name == 'Background':
                    wizard.yotech_layout = False
                    wizard.yotech_back_layout = True
                else:
                    wizard.yotech_layout = False
                    wizard.yotech_back_layout = False
