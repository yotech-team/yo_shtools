# -*- coding: utf-8 -*-

{
    "name": "Report Layout Integration",
    "category": "report",
    "summary": "Report Layout Integration",
    "author": "Nicolas Trubert",
    "website": "",
    "version": "17.0.1.0.0",
    "sequence": 1,
    "license": "AGPL-3",
    "description": """
        This module add capabilities to configure document layout. Add three row in document footer.
    """,
    "depends": [
        "base",
        "account",
        "sale_management",
    ],
    "data": [
        "views/external_layout_yotech.xml",
        "views/external_layout_clean.xml",
        "views/external_layout_standard.xml",
        "views/external_layout_stripped.xml",
        "views/res_company_views.xml",
        "data/report_layout.xml",
    ],
    "assets": {
        "web.report_assets_common": [
            "report_design_integration/static/src/scss/layout_clean.scss",
            "report_design_integration/static/src/scss/layout_yotech_design.scss",
        ],
    },
    "installable": True,
    "application": True,
    "auto_install": False,
}
