# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, api, _
import os
import logging
import re
from os import path
from odoo.tools import config

_logger = logging.getLogger(__name__)


class reset_confirm(models.TransientModel):
    _name = "reset.confirm"
    _description = "Reset Confirm"

    def confirm(self):
        filestore_dir_vdir = os.path.join(config['data_dir'], "filestore")
        config_obj = self.env['res.config.settings'].get_values()
        database_with = config_obj.get("reset_database_with")
        if database_with:
            paths = filestore_dir_vdir
            file = "do_reset.txt"
            dir_list = os.listdir(paths)
            # Creates a new file
            with open(os.path.join(paths, file), 'w') as fp:
                fp.write("%s" % database_with)
                pass
            dir_list = os.listdir(paths)
