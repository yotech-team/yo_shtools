# -*- coding: utf-8 -*-
# Nicolas Trubert
{
    "name": "Do Reset Database",
    "version": "17.0.1.0.0",
    "category": "Database",
    "summary": "Do Reset Database",
    "description": """
        Do Reset Database
    """,
    "author": "Nicolas Trubert",
    "website": "",
    "license": "AGPL-3",
    "depends": ["base_setup"],
    "data": [
        "security/ir.model.access.csv",
        "security/security.xml",
        "views/res_config_settings_view.xml",
        "wizard/reset_confirm_view.xml",
    ],
    "demo": [],
    "test": [],
    "installable": True,
    "auto_install": False,
}
