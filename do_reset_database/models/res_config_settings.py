# -*- coding: utf-8 -*-
from odoo import models, api, fields, _
import os
import logging
from os import path
from odoo.exceptions import UserError
from odoo.tools import config

_logger = logging.getLogger(__name__)


class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    reset_database_with = fields.Selection(
        [
            ("default", "Default"),
            ("backup", "Backup"),
            ("staging", "Staging"),
            ("other", "Other"),
        ],
        string="Reset Database For ",
    )

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        IrDefault = self.env["ir.default"].sudo()
        IrDefault.set(
            "res.config.settings", "reset_database_with", self.reset_database_with
        )
        return True

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        IrDefault = self.env["ir.default"].sudo()
        res.update(
            {
                "reset_database_with": IrDefault._get(
                    "res.config.settings", "reset_database_with"
                ),
            }
        )
        return res

    def do_reset(self):
        filestore_dir_vdir = os.path.join(config["data_dir"], "filestore")
        path_file = path.exists("%s/do_reset.txt" % (filestore_dir_vdir))
        if path_file:
            file = open("%s/do_reset.txt" % (filestore_dir_vdir), "r")
            for line in file.readlines():
                config_obj = self.env["res.config.settings"].get_values()
                if line == config_obj.get("reset_database_with"):
                    raise UserError(
                        _(
                            'Already exists do_reset file with value "%s"'
                            % config_obj.get("reset_database_with")
                        )
                    )
                else:
                    return {
                        "name": _("Reset Database"),
                        "type": "ir.actions.act_window",
                        "view_type": "form",
                        "view_mode": "form",
                        "res_model": "reset.confirm",
                        "views": [
                            (
                                self.env.ref("do_reset_database.view_reset_confirm").id,
                                "form",
                            )
                        ],
                        "view_id": self.env.ref(
                            "do_reset_database.view_reset_confirm"
                        ).id,
                        "target": "new",
                        "res_id": False,
                    }
        else:
            return {
                "name": _("Reset Database"),
                "type": "ir.actions.act_window",
                "view_type": "form",
                "view_mode": "form",
                "res_model": "reset.confirm",
                "views": [
                    (self.env.ref("do_reset_database.view_reset_confirm").id, "form")
                ],
                "view_id": self.env.ref("do_reset_database.view_reset_confirm").id,
                "target": "new",
                "res_id": False,
            }
